> 592110151 Siripan Saecheah
> 
> 592110151 ศิริพรรณ แซ่เชี๊ยะ


![May](https://www.img.in.th/images/88022df9de524c1f16edba163be91ced.md.jpg)

>  Action Plan

**Movie**
* [X]  Get All Cinema
* [ ]  Get Booking History
* [ ]  Get All Showtime by Movie Id
* [X]  Get All Showtime
* [ ]  Get One Showtime
* [X]  Get All Movies

**User**
* [X]  Change Image
* [X]  Change Info
* [ ]  Change Password
* [ ]  Get User Data
* [X]  Login
* [X]  Register

>  Class Diagram

![28a3e96ce52af3a3b986a7a81bd03428.jpg](https://www.img.in.th/images/28a3e96ce52af3a3b986a7a81bd03428.jpg)

