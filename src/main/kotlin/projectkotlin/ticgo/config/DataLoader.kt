//package projectkotlin.ticgo.config
//
//import org.apache.poi.ss.usermodel.CellType
//import org.apache.poi.ss.usermodel.Row
//import org.apache.poi.ss.util.CellReference
//import org.apache.poi.ss.util.CellUtil
//import org.apache.poi.xssf.usermodel.XSSFWorkbook
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.beans.factory.annotation.Value
//import org.springframework.core.io.ClassPathResource
//import org.springframework.stereotype.Component
//import projectkotlin.ticgo.entity.Cinema
//import projectkotlin.ticgo.entity.Seat
//import projectkotlin.ticgo.entity.SeatType
//import projectkotlin.ticgo.repository.CinemaRepository
//import projectkotlin.ticgo.repository.SeatRepository
//import java.io.IOException
//import javax.transaction.Transactional
//
//@Component
//class DataLoader {
//    @Value("\${xlsxPath}")
//    val xlsxPath: String? = null
//    @Autowired
//    lateinit var seatRepository: SeatRepository
//    @Autowired
//    lateinit var cinemaRepository: CinemaRepository
//
//
//    fun loadData() {
//        createCinemaData()
//        createSeatData()
//    }
//
//    fun getCellData(row: Row, col: String): String {
//        val colIdx = CellReference.convertColStringToIndex(col)
//        return getCellData(row, colIdx)
//    }
//
//    fun getCellData(row: Row, colIdx: Int): String {
//        val cell = CellUtil.getCell(row, colIdx)
//        if (cell.cellType == CellType.STRING) {
//            if (cell.stringCellValue == "NULL")
//                return ""
//            return cell.stringCellValue
//        } else if (cell.cellType == CellType.NUMERIC) {
//            cell.cellType = CellType.STRING
//            return cell.stringCellValue
//        }
//        return ""
//    }
//
//    @Transactional
//    fun createCinemaData() {
//        try {
//            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
//            val workbook = XSSFWorkbook(file)
//            val sheet = workbook.getSheet("cinema")
//            val rowIterator = sheet.iterator()
//            rowIterator.next()
//            while (rowIterator.hasNext()) {
//                val row = rowIterator.next()
//                if (getCellData(row, "A") != "") {
//                    cinemaRepository.save(Cinema(name = getCellData(row, "A")))
//                }
//            }
//        } catch (e: IOException) {
//
//        }
//    }
//
//    @Transactional
//    fun createSeatData() {
//        try {
//            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
//            val workbook = XSSFWorkbook(file)
//            val sheet = workbook.getSheet("seat")
//            val rowIterator = sheet.iterator()
//            rowIterator.next()
//            while (rowIterator.hasNext()) {
//                val row = rowIterator.next()
//                if (getCellData(row, "A") != "") {
//                    seatRepository.save(Seat(name = getCellData(row, "A"),
//                            seatType = SeatType.valueOf(getCellData(row, "B")),
//                            price = getCellData(row, "C").toDouble(),
//                            rows = getCellData(row, "D").toInt(),
//                            columns = getCellData(row, "E").toInt()
//                            ))
//                }
//            }
//        } catch (e: IOException) {
//
//        }
//    }
////    @Transactional
////    fun createProductData() {
////        try {
////            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
////            val workbook = XSSFWorkbook(file)
////            val sheet = workbook.getSheet("product")
////            val rowIterator = sheet.iterator()
////            rowIterator.next()
////            while (rowIterator.hasNext()) {
////                val row = rowIterator.next()
////                if (getCellData(row, "A") != "") {
////                    productRepository.save(Product(name = getCellData(row, "A"),
////                            description = getCellData(row, "B"),
////                            imageUrl = getCellData(row, "C"),
////                            price = getCellData(row, "D").toDouble(),
////                            amountInStock = getCellData(row, "E").toInt(),
////                            manufacturer = manufacturerRepository.findByName(getCellData(row, "F"))))
////                }
////            }
////        } catch (e: IOException) {
////
////        }
////    }
//}