package projectkotlin.ticgo.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import projectkotlin.ticgo.entity.*
import projectkotlin.ticgo.repository.*
import projectkotlin.ticgo.security.entity.Authority
import projectkotlin.ticgo.security.entity.AuthorityName
import projectkotlin.ticgo.security.entity.JwtUser
import projectkotlin.ticgo.security.repository.AuthorityRepository
import projectkotlin.ticgo.security.repository.UserRepository
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@Component
class ApplicationLoader:ApplicationRunner{
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var soundtrackRepository: SoundtrackRepository
    @Autowired
    lateinit var subtitleRepository: SubtitleRepository
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeReporitory
    @Autowired
    lateinit var selectedSeatRepository:SelectedSeatRepository
    @Autowired
    lateinit var seatInShowtimeRepository: SeatInShowtimeRepository
    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = UserData(firstName = "สมชาติ", email = "a@b.com",lastName = "อิอิ",password = "password")
        val custJwt = JwtUser(
                username = "admin",
                password = encoder.encode("password"),
                email = custl.email,
                enabled = true,
                firstname = custl.firstName,
                lastname = custl.lastName
        )
        userDataRepository.save(custl)
        userRepository.save(custJwt)
        custl.jwtUser = custJwt
        custJwt.user = custl
        custJwt.authorities.add(auth1)

    }


    @Transactional
    override fun run(args: ApplicationArguments?) {

        var premium= seatRepository.save(Seat("Premium", SeatType.PREMIUM,190.00,4,20))
        var deluxe= seatRepository.save(Seat("Deluxe",SeatType.DELUXE,150.00,10,20))
        var sofa= seatRepository.save(Seat("Sofa Sweet (Pair)",SeatType.SOFA,500.00,1,6))

        var cinema1 = cinemaRepository.save(Cinema("Cinema1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema3"))

        cinema1.seat = mutableListOf(sofa,premium,deluxe)
        cinema2.seat = mutableListOf(premium,deluxe)
        cinema3.seat = mutableListOf(sofa,premium,deluxe)

        var soundtrackEN = soundtrackRepository.save(Soundtrack("EN"))
        var soundtrackTH = soundtrackRepository.save(Soundtrack("TH"))

        var subtitleEN = subtitleRepository.save(Subtitle("EN"))
        var subtitleTH = subtitleRepository.save(Subtitle("TH"))

        var movie1 = movieRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล", 125
                , "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        var movie2 = movieRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3", 105
                , "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        var movie3 = movieRepository.save(Movie("Captain Marvel", 130
                , "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        var movie4 = movieRepository.save(Movie("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน", 120
                , "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))
        var movie5 = movieRepository.save(Movie("สุขสันต์วันตาย 2U", 100
                , "https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024"))
        var movie6 = movieRepository.save(Movie("คุซามะ อินฟินิตี้", 80
                , "https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024"))
        var movie7 = movieRepository.save(Movie("คนเหนือมนุษย์", 130
                , "https://lh3.googleusercontent.com/rVCTrkvoC4Iwpv4sPY8gSGYom0rKm28exIaVBD2sGYUip0SMuKqfDxyAVrd_Ps_H39Ss-NGT1nzytQPuBunr=w1024"))

        movie1.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie1.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie2.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie2.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie3.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie3.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie4.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie4.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie5.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie5.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie6.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie6.subtitles = mutableListOf(subtitleEN,subtitleTH)
        movie7.soundtracks = mutableListOf(soundtrackEN,soundtrackTH)
        movie7.subtitles = mutableListOf(subtitleEN,subtitleTH)

        var user1 = userDataRepository.save(UserData("may@g.com","1234","Siripan","Saecheah",
                "https://firebasestorage.googleapis.com/v0/b/dv-firebase-d6cd3.appspot.com/o/059ea36c-6841-4b88-bdbd-b5bcd2c8e6e5.jpg?alt=media"))

        var showtimeOfMovie1_1 = showtimeRepository.save(Showtime(Timestamp(1553396400000).time, Timestamp(1553393100000).time))

        showtimeOfMovie1_1.movie = movie1
        showtimeOfMovie1_1.cinema = cinema1

        var row:Char = 'A'

        for ((index, item) in showtimeOfMovie1_1.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowtime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowtimeRepository.save(SeatInShowtime(false, "${row}${row}", "${indexC}"))
                    }
                    else {
                        seat = seatInShowtimeRepository.save(SeatInShowtime(false, "${row}", "${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtimeOfMovie1_1.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }

        loadUsernameAndPassword()




    }

}