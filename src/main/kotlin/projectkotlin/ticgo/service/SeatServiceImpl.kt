package projectkotlin.ticgo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.ticgo.dao.SeatDao
import projectkotlin.ticgo.entity.Seat

@Service
class SeatServiceImpl:SeatService{
    override fun getSeats(): List<Seat> {
        return seatDao.getSeats()
    }

    @Autowired
    lateinit var seatDao: SeatDao
}