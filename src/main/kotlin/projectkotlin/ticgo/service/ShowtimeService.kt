package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.Showtime

interface ShowtimeService{
    fun getAllShowtimes(): List<Showtime>

}