package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.Seat

interface SeatService{
    fun getSeats():List<Seat>
}