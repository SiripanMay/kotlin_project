package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.Cinema

interface CinemaService{
    fun getAllCinema(): List<Cinema>

}