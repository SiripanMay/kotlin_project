package projectkotlin.ticgo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.ticgo.dao.BookingDao
import projectkotlin.ticgo.dao.SeatInShowtimeDao
import projectkotlin.ticgo.dao.ShowtimeDao
import projectkotlin.ticgo.dao.UserDataDao
import projectkotlin.ticgo.entity.Booking
import projectkotlin.ticgo.entity.SeatInShowtime
import projectkotlin.ticgo.entity.UserData

@Service
class BookingServiceImpl:BookingService{
    override fun bookingHistoryByUserId(id: Long): List<Booking> {
        return bookingDao.bookingHistoryByUserId(id)
    }

    override fun save(booking: Booking): Booking {
        var showtimeId = showtimeDao.findById(booking.showtimeId)
        var userId = userDataDao.findById(booking.userId)
        var seats = mutableListOf<SeatInShowtime>()
        for( item in booking.seats) {
            var seat = seatInShowtimeDao.findById(item.id)
            seat.status = true
            seats.add(seatInShowtimeDao.save(seat))
        }
        var book = Booking()
        book.userId = userId.id
        book.showtimeId = showtimeId.id
        book.seats = seats
        book.createdDateTime = System.currentTimeMillis()
        bookingDao.save(book)
        return book
    }

    @Autowired
    lateinit var bookingDao: BookingDao
    @Autowired
    lateinit var showtimeDao:ShowtimeDao
    @Autowired
    lateinit var seatInShowtimeDao:SeatInShowtimeDao
    @Autowired
    lateinit var userDataDao: UserDataDao
}