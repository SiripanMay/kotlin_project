package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.Booking

interface BookingService{
    fun save(booking: Booking): Booking
    fun bookingHistoryByUserId(id: Long): List<Booking>

}