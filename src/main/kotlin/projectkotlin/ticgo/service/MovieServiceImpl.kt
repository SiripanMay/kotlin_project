package projectkotlin.ticgo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.ticgo.dao.MovieDao
import projectkotlin.ticgo.entity.Movie
import javax.transaction.Transactional

@Service
class MovieServiceImpl:MovieService{
    @Transactional
    override fun remove(id: Long): Movie? {
        val movie = movieDao.findById(id)
        movie?.isDeleted = true
        return movie
    }

    override fun getAllMovies(): List<Movie> {
        return movieDao.getAllMovies()
    }

    @Autowired
    lateinit var movieDao: MovieDao
}