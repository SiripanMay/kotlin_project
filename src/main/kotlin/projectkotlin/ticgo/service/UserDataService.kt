package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.UserData
import projectkotlin.ticgo.entity.dto.UserImageDto
import projectkotlin.ticgo.entity.dto.UserInfoDto
import projectkotlin.ticgo.entity.dto.UserRegisterDto

interface UserDataService{
//    fun save(userRegister: UserData): UserData
    fun saveInfo(userInfoDto: UserInfoDto): UserData
    fun saveImage(id: Long, imageUrl: String):UserData
    abstract fun save(userRegisterDto: UserData): UserData
    fun findUserById(id: Long): UserData
//    fun save(userRegisterDto: UserRegisterDto): UserData


}
