package projectkotlin.ticgo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import projectkotlin.ticgo.dao.UserDataDao
import projectkotlin.ticgo.entity.UserData
import projectkotlin.ticgo.entity.dto.UserInfoDto
import projectkotlin.ticgo.entity.dto.UserRegisterDto
import projectkotlin.ticgo.repository.UserDataRepository
import projectkotlin.ticgo.security.entity.Authority
import projectkotlin.ticgo.security.entity.AuthorityName
import projectkotlin.ticgo.security.entity.JwtUser
import projectkotlin.ticgo.security.repository.AuthorityRepository
import projectkotlin.ticgo.security.repository.UserRepository
import projectkotlin.ticgo.util.MapperUtil

@Service
class UserDataServiceImpl:UserDataService{
    override fun findUserById(id: Long): UserData {
        var user = userDataDao.findById(id)
        return user
    }

    override fun save(userRegisterDto: UserData): UserData {
        return userDataDao.save(userRegisterDto)
    }

    override fun saveImage(id: Long, imageUrl: String): UserData {
        val user = userDataDao.findById(id)
        user.image = imageUrl
        return userDataDao.save(user)
    }

    override fun saveInfo(userInfoDto: UserInfoDto): UserData {
        val user = userDataDao.findById(userInfoDto.id)
        user.firstName = userInfoDto.firstName
        user.lastName = userInfoDto.lastName
        return userDataDao.save(user)
    }



    @Autowired
    lateinit var userDataDao:UserDataDao
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userDataRepository: UserDataRepository
}