package projectkotlin.ticgo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.ticgo.dao.CinemaDao
import projectkotlin.ticgo.entity.Cinema

@Service
class CinemaServiceImpl:CinemaService{
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }
    @Autowired
    lateinit var cinemaDao: CinemaDao
}