package projectkotlin.ticgo.service

import projectkotlin.ticgo.entity.Movie

interface MovieService{
    fun getAllMovies(): List<Movie>
    fun remove(id: Long): Movie?

}