package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.Movie
import projectkotlin.ticgo.repository.MovieRepository

@Profile("db")
@Repository
class MovieDaoDBImpl:MovieDao{
    override fun findById(id: Long): Movie {
        return movieRepository.findById(id).orElse(null)
    }

    override fun getAllMovies(): List<Movie> {
        return movieRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var movieRepository: MovieRepository
}