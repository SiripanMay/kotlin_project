package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.Cinema

interface CinemaDao{
    fun getAllCinema(): List<Cinema>
}