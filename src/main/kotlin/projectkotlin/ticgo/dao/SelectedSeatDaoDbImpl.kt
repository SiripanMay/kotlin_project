package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.repository.SelectedSeatRepository

@Profile("db")
@Repository
class SelectedSeatDaoDbImpl:SelectedSeatDao{
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
}