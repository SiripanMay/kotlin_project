package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.UserData
import projectkotlin.ticgo.entity.dto.UserInfoDto
import projectkotlin.ticgo.repository.UserDataRepository
import projectkotlin.ticgo.security.entity.Authority
import projectkotlin.ticgo.security.entity.AuthorityName
import projectkotlin.ticgo.security.entity.JwtUser
import projectkotlin.ticgo.security.repository.AuthorityRepository
import projectkotlin.ticgo.security.repository.UserRepository

@Profile("db")
@Repository
class UserDataDaoDBImpl:UserDataDao{
    override fun findById(id: Long?): UserData {
        return userDataRepository.findById(id!!).orElse(null)
    }

    override fun save(userRegister: UserData): UserData {
        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(roleUser)
        val encoder = BCryptPasswordEncoder()
        val custl = userRegister
        val custJwt = JwtUser(
                username = custl.email,
                password = encoder.encode(custl.password),
                email = custl.email,
                enabled = true,
                firstname = custl.firstName,
                lastname = custl.lastName
        )
        userDataRepository.save(custl)
        userRepository.save(custJwt)
        custl.jwtUser = custJwt
        custJwt.user = custl
        custJwt.authorities.add(roleUser)

        return userDataRepository.save(userRegister)
    }

    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
}