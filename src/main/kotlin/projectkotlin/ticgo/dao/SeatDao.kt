package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.Seat

interface SeatDao{
    fun getSeats():List<Seat>
}