package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.SeatInShowtime

interface SeatInShowtimeDao{
    fun findById(id: Long?): SeatInShowtime
    fun save(seat: SeatInShowtime): SeatInShowtime


}