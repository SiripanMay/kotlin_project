package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.SeatInShowtime
import projectkotlin.ticgo.repository.SeatInShowtimeRepository
@Profile("db")
@Repository
class SeatInShowtimeDaoDBImpl:SeatInShowtimeDao{
    override fun findById(id: Long?): SeatInShowtime {
        return seatInShowtimeRepository.findById(id!!).orElse(null)
    }

    override fun save(seat: SeatInShowtime): SeatInShowtime {
        return seatInShowtimeRepository.save(seat)
    }

    @Autowired
    lateinit var seatInShowtimeRepository: SeatInShowtimeRepository

}