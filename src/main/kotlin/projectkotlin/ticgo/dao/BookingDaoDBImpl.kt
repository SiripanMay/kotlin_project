package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.Booking
import projectkotlin.ticgo.repository.BookingRepository

@Profile("db")
@Repository
class BookingDaoDBImpl:BookingDao{
    override fun bookingHistoryByUserId(id: Long): List<Booking> {
        var userId = userDataDao.findById(id)
        var book = Booking()
        book.userId = id

        return bookingRepository.findByUserId(id)
    }

    override fun findById(id: Long?): Booking {
        return bookingRepository.findById(id!!).orElse(null)
    }

    override fun save(book: Booking): Booking {
        return bookingRepository.save(book)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var userDataDao: UserDataDao
}