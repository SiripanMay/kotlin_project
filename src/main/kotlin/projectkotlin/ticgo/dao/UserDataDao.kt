package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.UserData
import projectkotlin.ticgo.entity.dto.UserInfoDto

interface UserDataDao {
//    fun save(userRegister: UserData): UserData
    fun findById(id: Long?): UserData

    fun save(user: UserData): UserData
}