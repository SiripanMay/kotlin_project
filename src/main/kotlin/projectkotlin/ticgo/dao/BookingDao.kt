package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.Booking

interface BookingDao{
    fun save(book: Booking):Booking
    fun findById(id: Long?): Booking
    fun bookingHistoryByUserId(id: Long): List<Booking>

}