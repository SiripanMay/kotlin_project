package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.Movie

interface MovieDao{
    fun getAllMovies(): List<Movie>
    fun findById(id: Long): Movie

}