package projectkotlin.ticgo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.Cinema
import projectkotlin.ticgo.entity.Seat
import projectkotlin.ticgo.repository.CinemaRepository

@Profile("db")
@Repository
class CinemaDaoDBImpl:CinemaDao{
    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
}