package projectkotlin.ticgo.dao

import org.jetbrains.annotations.NotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.ticgo.entity.Showtime
import projectkotlin.ticgo.repository.SeatInShowtimeRepository
import projectkotlin.ticgo.repository.ShowtimeReporitory

@Profile("db")
@Repository
class ShowtimeDaoDBImpl:ShowtimeDao{
    override fun findById(id: Long?): Showtime {
        return showtimeRepository.findById(id!!).orElse(null)
    }

    override fun getAllShowtimes(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeReporitory
}