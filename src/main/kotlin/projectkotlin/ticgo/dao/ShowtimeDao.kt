package projectkotlin.ticgo.dao

import projectkotlin.ticgo.entity.Showtime

interface ShowtimeDao{
    fun getAllShowtimes(): List<Showtime>
    abstract fun findById(id: Long?): Showtime
//    fun findById(id: Long?): Showtime

}