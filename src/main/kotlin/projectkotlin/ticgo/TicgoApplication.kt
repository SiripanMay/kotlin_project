package projectkotlin.ticgo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TicgoApplication

fun main(args: Array<String>) {
    runApplication<TicgoApplication>(*args)
}
