package projectkotlin.ticgo.security.contoller

data class JwtAuthenticationRequest(var username: String? = null,
                                    var password: String? = null)