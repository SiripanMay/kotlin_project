package projectkotlin.ticgo.security.contoller

data class JwtAuthenticationResponse(
        var token: String? = null
)