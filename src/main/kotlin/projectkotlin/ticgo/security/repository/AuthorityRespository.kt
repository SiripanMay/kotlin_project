package projectkotlin.ticgo.security.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.security.entity.Authority
import projectkotlin.ticgo.security.entity.AuthorityName

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}