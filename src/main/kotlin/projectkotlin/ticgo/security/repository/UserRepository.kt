package projectkotlin.ticgo.security.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.security.entity.JwtUser

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}