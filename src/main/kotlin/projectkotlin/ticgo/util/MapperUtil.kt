package projectkotlin.ticgo.util

import org.mapstruct.InheritConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import projectkotlin.ticgo.entity.*
import projectkotlin.ticgo.entity.dto.*
import projectkotlin.ticgo.security.entity.Authority

@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANT = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "seat", target = "seats")
    )
    fun mapCinemaDto(cinema: Cinema):CinemaDto
    fun mapCinemaDto(cinemas: List<Cinema>):List<CinemaDto>

    fun mapSeatDto(seat: Seat):SeatDto
    fun mapSeatDto(seat: List<Seat>):List<SeatDto>

    fun mapMovieDto(movie: Movie):MovieDto
    fun mapMovieDto(movie: List<Movie>):List<MovieDto>

    fun mapShowtimeDto(showtime: Showtime):ShowtimeDto
    fun mapShowtimeDto(showtime: List<Showtime>):List<ShowtimeDto>

    fun mapSelectSeatDto(selectedSeat: SelectedSeat):SelectedSeatDto
    fun mapSelectSeatDto(selectedSeat: List<SelectedSeat>):List<SelectedSeatDto>

    fun mapUserDataDto(userData: UserData):UserDataDto

    fun mapUserInfoDto(userData: UserData):UserInfoDto
    fun mapUserImageDto(userData: UserData):UserImageDto
    fun mapUserRegisterDto(userData: UserData):UserRegisterDto

    @InheritConfiguration
    fun mapUserInfoDto(userInfoDto: UserInfoDto):UserData

    @InheritConfiguration
    fun mapUserRegisterDto(userRegisterDto: UserRegisterDto):UserData


//    @Mappings(
//            Mapping(source = "userData.jwtUser.username",target = "username"),
//            Mapping(source = "userData.jwtUser.authorities",target = "authorities")
//    )
    fun mapUserDatas(userData: UserData):UserLoginDto

//    fun mapUserRegisterDto(userData: UserData):UserRegisterDto

    fun mapAuthority(authority: Authority):AuthorityDto
    fun mapAuthority(authority: List<Authority>):List<AuthorityDto>


}


