package projectkotlin.ticgo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Seat(
        var name:String?=null,
        var type:SeatType?=null,
        var price:Double?=null,
        @Column(name = "seat_rows")
        var rows:Int?=null,
        @Column(name = "seat_coloumns")
        var columns:Int?=null
){
    @Id
    @GeneratedValue
    var id:Long?=null
}

