package projectkotlin.ticgo.entity

import java.util.*
import javax.persistence.*

@Entity
data class Showtime(
        var startDateTime:Long?=null,
        var endDateTime:Long?=null
){
    @Id
    @GeneratedValue
    var id:Long?=null
    @ManyToOne
    lateinit var movie: Movie
    @OneToOne
    lateinit var cinema: Cinema
    @OneToMany
    var seats = mutableListOf<SelectedSeat>()
}