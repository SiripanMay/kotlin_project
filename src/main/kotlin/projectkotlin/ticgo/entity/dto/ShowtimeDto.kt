package projectkotlin.ticgo.entity.dto

data class ShowtimeDto(
        var startDateTime:Long?=null,
        var endDateTime:Long?=null,
        var movie: MovieDto?=null,
        var cinema: CinemaDto?=null,
        var seats : List<SelectedSeatDto>?=null
)

