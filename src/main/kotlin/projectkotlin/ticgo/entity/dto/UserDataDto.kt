package projectkotlin.ticgo.entity.dto

data class UserDataDto(
        var email:String?=null,
//        var password:String?=null,
        var firstName:String?=null,
        var lastName:String?=null,
        var image:String?=null
)

data class UserInfoDto(
        var firstName:String?=null,
        var lastName:String?=null,
        var id:Long?=null
)

data class UserImageDto(
        var image:String?=null,
        var id:Long?=null
)

data class UserLoginDto(
        var id:Long?=null,
        var email:String?=null,
        var firstName:String?=null,
        var lastName:String?=null
)

