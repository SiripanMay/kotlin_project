package projectkotlin.ticgo.entity.dto

import projectkotlin.ticgo.entity.Seat
import projectkotlin.ticgo.entity.SeatInShowtime

data class SelectedSeatDto(
        var seatDetail: Seat? = null,
        var seats : List<SeatInShowtimeDto>?=null
)

data class SeatInShowtimeDto(
        var status: Boolean = false,
        var row: String? = null,
        var column: String? = null
)