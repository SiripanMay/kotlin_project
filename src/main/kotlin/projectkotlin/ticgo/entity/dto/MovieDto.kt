package projectkotlin.ticgo.entity.dto

import projectkotlin.ticgo.entity.Soundtrack
import projectkotlin.ticgo.entity.Subtitle

data class MovieDto(
        var name:String?=null,
        var duration:Int?=null,
        var image:String?=null,
        var soundtracks: Array<SoundtrackDto> ?=null,
        var subtitles: List<SubtitleDto>?=null
)

data class SoundtrackDto(
        var soundtrack: String?=null
)

data class SubtitleDto(
        var subtitle: String?=null
)