package projectkotlin.ticgo.entity.dto

import projectkotlin.ticgo.entity.SeatType

data class SeatDto(
        var name:String?=null,
        var type: SeatType?=null,
        var price:Double?=null,
        var rows:Int?=null,
        var columns:Int?=null
)