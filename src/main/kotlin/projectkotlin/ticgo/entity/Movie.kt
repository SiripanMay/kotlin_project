package projectkotlin.ticgo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Movie(
        var name:String?=null,
        var duration:Int?=null,
        var image:String?=null,
        var isDeleted:Boolean=false
){
    @Id
    @GeneratedValue
    var id:Long?=null
    @ManyToMany
    var soundtracks = mutableListOf<Soundtrack>()
    @ManyToMany
    var subtitles = mutableListOf<Subtitle>()
}



