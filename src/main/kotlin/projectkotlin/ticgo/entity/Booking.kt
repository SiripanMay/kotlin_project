package projectkotlin.ticgo.entity

import java.util.*
import javax.persistence.*

@Entity
data class Booking(
        var userId:Long?=null,
        var showtimeId :Long?=null,
        var createdDateTime:Long?=null
){
    @Id
    @GeneratedValue
    var id:Long?=null
    @ManyToMany
    var seats = mutableListOf<SeatInShowtime>()
}