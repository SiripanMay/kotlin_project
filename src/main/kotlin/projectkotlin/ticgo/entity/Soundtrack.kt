package projectkotlin.ticgo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Soundtrack(var soundtrack: String?=null){
    @Id
    @GeneratedValue
    var id:Long?=null
}
