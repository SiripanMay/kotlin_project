package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import projectkotlin.ticgo.entity.dto.UserImageDto
import projectkotlin.ticgo.entity.dto.UserInfoDto
import projectkotlin.ticgo.service.AmazonClient
import projectkotlin.ticgo.service.UserDataService
import projectkotlin.ticgo.util.MapperUtil

@RestController
class BucketController {
    @Autowired
    lateinit var amazonClient: AmazonClient
    @Autowired
    lateinit var userDataService: UserDataService

    @PostMapping("/uploadFile")
    fun uploadFile(@RequestPart(value = "file") file: MultipartFile): ResponseEntity<*> {
        return ResponseEntity.ok(this.amazonClient.uploadFile(file))
    }

    @DeleteMapping("/deleteFile")
    fun deleteFile(@RequestPart(value = "url") fileUrl: String): ResponseEntity<*> {
        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
    }
}
