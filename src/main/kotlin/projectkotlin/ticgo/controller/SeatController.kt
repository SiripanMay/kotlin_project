package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.ticgo.service.SeatService

@RestController
class SeatController{
    @Autowired
    lateinit var seatService: SeatService

    @GetMapping("/seat")
    fun getSeats(): ResponseEntity<Any> {
        return ResponseEntity.ok(seatService.getSeats())
    }
}