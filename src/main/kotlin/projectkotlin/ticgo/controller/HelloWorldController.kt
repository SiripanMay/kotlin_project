package projectkotlin.ticgo.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloWorldController{
    @GetMapping("/helloWorld")
    fun getHelloWorld(): String {
        return "HelloWorld"
    }
}
