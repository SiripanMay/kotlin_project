package projectkotlin.ticgo.controller

import org.apache.catalina.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import projectkotlin.ticgo.entity.UserData
import projectkotlin.ticgo.entity.dto.UserInfoDto
import projectkotlin.ticgo.entity.dto.UserRegisterDto
import projectkotlin.ticgo.security.JwtTokenUtil
import projectkotlin.ticgo.service.AmazonClient
import projectkotlin.ticgo.service.UserDataService
import projectkotlin.ticgo.util.MapperUtil
import sun.audio.AudioDevice.device

@RestController
class UserDataController{
    @Autowired
    lateinit var userDataService: UserDataService
    @Autowired
    lateinit var jwtTokenUtil: JwtTokenUtil

    @PostMapping("/users/register")
    fun register(@RequestBody userData:UserData):ResponseEntity<Any>{
        val output = userDataService.save(userData)
        val outputDto= MapperUtil.INSTANT.mapUserRegisterDto(output)
        return ResponseEntity.ok(outputDto)
    }


    @PutMapping("/user/edit/data")
    fun editUser(@RequestBody userInfoDto: UserInfoDto):ResponseEntity<Any>{
        val output= MapperUtil.INSTANT.mapUserInfoDto(userDataService.saveInfo(userInfoDto))
        return ResponseEntity.ok(output)

    }

    @GetMapping("/user/info/{userId}")
    fun getUserById(@PathVariable("userId")id:Long ): ResponseEntity<Any> {
        var output = MapperUtil.INSTANT.mapUserDataDto(userDataService.findUserById(id))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @Autowired
    lateinit var amazonClient: AmazonClient

    @PostMapping("/user/image/{userId}")
    fun uploadFile(@RequestPart(value = "file") file: MultipartFile,
                   @PathVariable ("userId")id:Long ): ResponseEntity<*> {
        var imageUrl = this.amazonClient.uploadFile(file)
        var userId = userDataService.saveImage(id,imageUrl)
        val output= MapperUtil.INSTANT.mapUserImageDto(userId)
        return ResponseEntity.ok(output)
    }

}