package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import projectkotlin.ticgo.entity.Booking
import projectkotlin.ticgo.service.BookingService
import projectkotlin.ticgo.util.MapperUtil

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking")
    fun postBooking(@RequestBody booking: Booking): ResponseEntity<Any> {
        val bookings = bookingService.save(booking)
        bookings?.let { ResponseEntity.ok(bookings) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/booking/history/{userId}")
    fun bookingHistory(@PathVariable ("userId")id:Long ): ResponseEntity<Any> {
        val book= bookingService.bookingHistoryByUserId(id)
        return ResponseEntity.ok(book)
    }

}