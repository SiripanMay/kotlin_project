package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.ticgo.service.ShowtimeService
import projectkotlin.ticgo.util.MapperUtil

@RestController
class ShowtimeController{
    @Autowired
    lateinit var showtimeService: ShowtimeService

    @GetMapping("/showtimes")
    fun getAllShowtimes():ResponseEntity<Any>{
        val showtimes = showtimeService.getAllShowtimes()
        return ResponseEntity.ok(MapperUtil.INSTANT.mapShowtimeDto(showtimes))
    }

}