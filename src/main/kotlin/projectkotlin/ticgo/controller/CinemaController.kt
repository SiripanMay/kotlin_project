package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.ticgo.entity.Cinema
import projectkotlin.ticgo.service.CinemaService
import projectkotlin.ticgo.util.MapperUtil

@RestController
class CinemaController{
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema(): ResponseEntity<Any> {
        val cinemas= cinemaService.getAllCinema()
        return ResponseEntity.ok(MapperUtil.INSTANT.mapCinemaDto(cinemas))
    }

}