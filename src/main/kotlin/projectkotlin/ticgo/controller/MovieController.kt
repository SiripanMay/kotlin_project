package projectkotlin.ticgo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import projectkotlin.ticgo.service.MovieService
import projectkotlin.ticgo.util.MapperUtil

@RestController
class MovieController{
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movies")
    fun getAllMovies():ResponseEntity<Any>{
        val movies = movieService.getAllMovies()
        return ResponseEntity.ok(MapperUtil.INSTANT.mapMovieDto(movies))
    }

    @DeleteMapping("deleted/{id}")
    fun deleteProduct(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val movie =movieService.remove(id)
        movie?.let { return ResponseEntity.ok(it)  }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }

}