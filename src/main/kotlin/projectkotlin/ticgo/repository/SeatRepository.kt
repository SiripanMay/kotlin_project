package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Seat

interface SeatRepository: CrudRepository<Seat, Long>