package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Cinema

interface CinemaRepository:CrudRepository<Cinema,Long>