package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.UserData

interface UserDataRepository:CrudRepository<UserData,Long> {

}