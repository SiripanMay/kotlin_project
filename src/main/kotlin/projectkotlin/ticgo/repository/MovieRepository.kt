package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Movie

interface MovieRepository:CrudRepository<Movie,Long> {
    fun findByIsDeletedIsFalse(): List<Movie>
}
