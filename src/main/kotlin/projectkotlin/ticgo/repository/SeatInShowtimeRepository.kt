package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.SeatInShowtime

interface SeatInShowtimeRepository: CrudRepository<SeatInShowtime, Long>