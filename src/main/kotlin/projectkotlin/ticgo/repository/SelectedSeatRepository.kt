package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.SelectedSeat

interface SelectedSeatRepository: CrudRepository<SelectedSeat, Long>