package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Subtitle

interface SubtitleRepository: CrudRepository<Subtitle,Long>