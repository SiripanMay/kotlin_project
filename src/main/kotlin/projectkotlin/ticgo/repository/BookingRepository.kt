package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Booking

interface BookingRepository:CrudRepository<Booking,Long>{
    fun findByUserId(id:Long):List<Booking>
}