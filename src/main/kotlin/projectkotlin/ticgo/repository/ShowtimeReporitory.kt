package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Showtime

interface ShowtimeReporitory:CrudRepository<Showtime,Long>