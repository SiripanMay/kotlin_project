package projectkotlin.ticgo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.ticgo.entity.Soundtrack

interface SoundtrackRepository:CrudRepository<Soundtrack,Long>